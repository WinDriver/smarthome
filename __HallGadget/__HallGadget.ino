//
// #############################################
// ##                                         ##
// ##          HALL CONTROLLER, v3.0          ##
// ##     Module firmware (ATMega328P uC)     ##  
// ##                                         ##
// #############################################
// ##                                         ##
// ##         (C)Copyright 2016 by            ##
// ##    Gorobchenko 'WinDriver' Michael      ##
// ##     mailto: admin@gorobchenko.tk        ##
// ##                                         ##
// ##  MIT Licence, Free for use and modify.  ##
// #############################################
//
// v1.0: @2016-Nov-18
// v1.1: @2016-Nov-24
// v1.2: @2016-Nov-27
//
// #############################################
//
//       :: OUTGOING Protocol definitions ::
//
//   U<cm>  - ultrasonic distance, ex. U100
//   B - Bell event, ex. B
//   D<1|0> 1 - open door, 0 - close door, ex. D1
//   P<1|0> 1 - PIR on,    0 - PIR off, ex. P1
//   C<count> - Comparator counter, ex. C15
//   I<interval> - Comparator last interval in microsec, ex. I12000
//   i<interval> - Comparator last interval in microsec, APPROX., ex. i11223
//   L<level> - Light level, ex. L777
//   R<datetime> - RTC clock, ex. R2016-Nov-21 MON 12:35:43
//   t<temp> - DS3231 temperature, ex. t26.00
//   T<temp> - DB18B20 temperature, ex. T24.00
//   K<keystate> - keystate in bin encoding, ex. K00010000
//   G<data> - data from EEPROM
//
//
//       ::  INGOING Protocol definitions ::
//
//   R - resend collected info (Light, Counter, RTC, Temp etc.), ex. R0
//   U<cm> - set event distance in cm for Ultrasonic sensor, ex. U200
//   D<dd,mm,yy> - set date to RTC, ex. D2016-11-22
//   T<hh,mm,ss> - set time to RTC, ex. T11:30:20
//   S<num,value> - set num output to value, ex. SA,1
//           Num: 1 - PWM1
//                2 - PWM2
//                3 - PWMLR
//                A - AUXOUT
//                B - BUZZER
//                L - BACKLIGHT
//           Value range: 0-32
//   A<num,value> - animate PWM from current state (setted by "S" command) to Value. 
//           Num: 1|2|3 (see "S" command description)
//   a<value> - set animation step count per second, ex. a20
//   B<value> - LED brightness, 0-7, ex. B2
//   L<2|1|0LedData> - set data to LED. 
//           LedCmd (1 byte):
//                0 - use microcontroller data
//                1 - use direct data from UART 
//                2 - use date from UART, using Charmap
//                LedData (4 bytes) - binary data
//   l<data>  - LED bulbs data, 10 bytes. Each byte configure bulbs state. Repeatly.
//   Z<data>  - Buzzer gamma data, 2 bytes. Each bit configure buzzer on/off. Not repeat.
//   F<herz>  - Timer 1 frequency, herz. Default 200., ex. F200;
//   P<bank|Data> - Put data to EEPROM, bank = 0|1|2|3 (char), bank length = 255, Data - char data
//   G<bank> - Get data from EEPROM, bank = 0|1|2|3 (char), bank length = 255

// #############################################

#include <TimerOne.h>
#include <PinChangeInt.h> 
#include <DS3231.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <TM1638.h>
#include <TM16XXFonts.h>
#include <EEPROM.h>

// * Timer 1 frequency
uint16_t timerFrequency = 200;

// Definitions
// (*) - may be changed
// Timer 1 interval
#define timerInterval          1000000/timerFrequency
// * PWM change events per second
#define pwmChangePerSecond     30
// PWM change event counter
#define pwmChangeMax           timerFrequency/pwmChangePerSecond
// * bulbs rotate events, per second
#define bulbsRotatePerSecond   8
// bulbs rotate counter trigger
#define bulbsRotateMax         timerFrequency/bulbsRotatePerSecond
// * buzzer rotate events, per second
#define buzzerRotatePerSecond  4
// buzzer rotate counter trigger
#define buzzerRotateMax        timerFrequency/buzzerRotatePerSecond
// * Ultrasound sensor scans per second
#define USscansPerSecond       2
// counter for Trigger Ultrasound
#define USscansMax             timerFrequency/USscansPerSecond  
// * send interval in seconds
#define sendInterval           60 
// counter for Send
#define sendIntervalMax        timerFrequency*sendInterval
// * thresholds for PIR sensor (3.3v approx. value is 675)
#define minThreshold           100
#define maxThreshold           500
// * DS18B20 resolution in bits
#define tempResolution         12
// DS18B20 conversation delay in milliseconds
#define tempDelay              750 / (1 << (12 - tempResolution))
// build date/time
#define buildVersion           __DATE__ __TIME__   

// Pin definitions
#define UW_ECHO    2
#define BELL       3
#define UW_TRIG    4
#define DOOR       5
#define TEMP       8
#define PWM1       9
#define PWM2       10
#define PWMLR      11
#define AUXOUT     12
#define BACKLIGHT  13
#define DIO        A0
#define STB        A1
#define CLK        A2
#define BUZZER     A3
#define PIR        A7
#define LIGHT      A6


// Interrupt flags and variables
// interrupt flags
volatile bool irCommand     = false; // Receive command via UART
volatile bool irUSecho      = false; // UltraSonic Echo interrupt
volatile bool irBell        = false; // BELL interrupt
volatile bool irDoor        = false; // DOOR interrupt
// event flags
volatile bool ifUStrig      = false; // Ultrasonic Trig flag
volatile bool ifSend        = false; // Send flag 
volatile bool ifTemp        = false; // Temp conversation in progress
volatile bool ifBuzzer      = false; // Buzzer playing flag
volatile byte ifAnimate     = 0;     // PWM animate flag. It's a BIT set flag!
volatile bool ifAnimateStep = false; // PWN step event
volatile unsigned long USscans       = 0;     // UltraSonic trigger pin knock counter (if it = USscansMax then knock trigger pin)


// Global variables
unsigned long startMicrosUS;              // start time for US
unsigned long stopMicrosUS;               // stop time for US
unsigned long prevMicrosCmp;              // previous time when Comparator triggered
unsigned long deltaMicrosCmp;             // last interval between Comparator triggered
unsigned long d1, d2, d3, d4, d5 = 0;     // variables for LPF for Comparator
unsigned long tempRequest = 0;            // time counter for async DS18B20 reading
float temp = 0.0;                         // last temperature of DS18B20
volatile unsigned long sendCounter = 0;   // counter when Send should work
volatile unsigned long cmpCounter = 0;    // Comparator pulses counter
volatile unsigned long bulbsCounter = 0;  // Bulbs rotate counter
volatile byte bulbCurrent, bulbPrev = 0;  // Bulb data index
volatile unsigned long buzzerCounter = 0; // buzzer index counter
volatile byte buzzCurrent, buzzPrev = 0;  // buzzer gamma index
uint16_t ringDistance = 150;              // < 150cm, then put outgoing message, may change programmatically
bool PIRstate = false;                    // current PIR state
char ledData[5] = {'0', 0, 0, 0, 0};      // LED data, last 4 symbols
byte bulbState[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // LED bulbs state. Changes in cycle.
word buzzerData;                          // buzzer gamma data. Bitset = 16 bits. bit = 0 - no sound, bit = 1 - play
byte keys, lastKeys = 0;                  // Readed keys value and last state
uint8_t pwmValues[3] = {0, 0, 0};         // Current PWM states. 
uint8_t pwmDest[3] = {0, 0, 0};           // Destination PWM states
unsigned long animateCounter = 0;         // counter for PWM animation
unsigned long animatePerSecond = 20;      // animation step per second, may change programmatically
unsigned long animateMax = timerFrequency/animatePerSecond;
String bufferData = "";                   // UART input data buffer
uint8_t bufferPtr = 0;                    // UART input data buffer current pointer
int p1,p2,p3;                             // temporary variables. Direct don't work!
char pc;

Time rtcTime;

// ----- Objects -----
DS3231 rtc(SDA, SCL);                     // RTC
OneWire tempSensor(TEMP);                 // DS18B20 sensor OneWire
DallasTemperature ds(&tempSensor);        // DS18B20 sensor DallasTemperature
TM1638 led(DIO, CLK, STB);                // TM1638 led/key display

// ===========================================
// Utilitary Functions
void dbg(String str) {
  Serial.print("-");
  Serial.println(str);
}

long bufferParseInt() {
  // bufferData - input buffer
  // bufferPtr  - current buffer position
  long i = 0;
  while (true) { // clear chars before Digit data
    if (bufferPtr >= bufferData.length()) return i;
    if (isDigit(bufferData[bufferPtr])) break;
    bufferPtr++;
  }
  while (true) {
    i = i*10 + bufferData[bufferPtr++] - '0';
    if (!isDigit(bufferData[bufferPtr]) || (bufferPtr >= bufferData.length())) return i;
  }
}

inline char bufferParseChar() {
  return bufferData[bufferPtr++];
}

inline void bufferClear() {
  bufferPtr = 0;
  bufferData = "";
}
  
inline int distance() { // calculate US distance
  return (stopMicrosUS - startMicrosUS)/58; // return distance in "cm"
}

inline void comparatorSetup() { // setup comparator
  ACSR  |= _BV(ACIE) | _BV(ACIS1) | _BV(ACIS0);  // Enable comparator interrupt, RASING front.
                                                 // if ACIS0 = 0 then FALLING front
                                                 // if ACIS0 = 0 and ACIS1 = 0 then RASING and FALLING fronts
  DIDR1 |= _BV(AIN1D) | _BV(AIN0D);              // Disable digital pins 6 and 7
}

inline void writeEEPROMString (char bank, String S) { // write String @addr 256, max length = 254
  int addr = bank - '0';
  addr = addr * 256;
  if ((addr < 0) || (addr > 768)) return; // Invalid bank No.
  byte len = S.length();
  EEPROM.write(addr++, len);
  for (uint8_t i = 0; i < len; i++) EEPROM.write(addr + i, S[i]);
}

inline String readEEPROMString (char bank) { // read String @addr 256, max length = 254
  String S = "";
  int addr = bank - '0';
  addr = addr * 256;
  if ((addr < 0) || (addr > 768)) return; // Invalid bank No.
  char c;
  byte len = EEPROM.read(addr++);
  for (uint8_t i = 0; i < len; i++) {
    c = EEPROM.read(addr + i);
    S += c;
  }
  return S;
}

inline void sendRTC() { // send RTC data to UART
  Serial.print("R");
  Serial.print(rtc.getDateStr(FORMAT_LONG, FORMAT_BIGENDIAN, '-'));
  Serial.print(" ");
  Serial.print(rtc.getDOWStr(FORMAT_SHORT));
  Serial.print(" ");
  Serial.println(rtc.getTimeStr(FORMAT_LONG));
  Serial.print("t");
  Serial.println(rtc.getTemp(), 2);
}

inline void updateBuzzer() {
  if (buzzCurrent != buzzPrev) {
    if (buzzCurrent > 16) {
      ifBuzzer = false;
      buzzCurrent = 0;
      buzzPrev = 0;
      digitalWrite(BUZZER, LOW);
      return;
    }
    buzzPrev = buzzCurrent;
    digitalWrite(BUZZER, bitRead(buzzerData, buzzCurrent - 1)); // play gamma index
  }
}

inline uint16_t pwmValue(int val) {
  uint16_t tv = val*val;
  if (val < 0) return 0;
  else
  if (tv > 1023) return(1023);
  else return(tv);
}

inline void updateAnimate() {
  uint8_t valv, vald;
  for (uint8_t i = 0; i < 3; i++) { // cycle index: 0,1,2
    if (bitRead(ifAnimate, i)) {
      if (pwmDest[i] > pwmValues[i]) { // need increase pwm
        pwmValues[i]++;
      }
      else
      if (pwmDest[i] < pwmValues[i]) { // need decrease pwm
        pwmValues[i]--;
      }

      updatePins(i + 49, pwmValues[i]); // Convert index i to string value "1"

      if (pwmDest[i] == pwmValues[i]) { // stop animate
        bitClear(ifAnimate, i);
      }
    }
  }
  ifAnimateStep = false;
}

inline void updateLed () { // update Led indicator and check for Keys pressed
  int i;
  static char output[] = "        ";             // LED buffer, length = 8 byte
  rtcTime = rtc.getTime();                       // Time from RTC
  if (rtcTime.hour < 10)                         // tens of hour
    output[0] = 32;                              // ...if < 10 then blank
  else
    output[0] = char((rtcTime.hour / 10) + 48);
  output[1] = char((rtcTime.hour % 10) + 48);    // units of hour
  if (rtcTime.min < 10)                          // tens of minutes
    output[2] = 48;
  else
    output[2] = char((rtcTime.min / 10) + 48);
  output[3] = char((rtcTime.min % 10) + 48);     // units of minutes
  // convert first 4 characters to LED
  
  if (ledData[0] == '0') { // fill LED data from microcontroller
    i = round(temp);
    output[4] = 32; // write temperature. Negative temperature ignored.
    output[5] = char((i / 10) + 48); // tens of temp
    output[6] = char((i % 10) + 48); // units ot temp
    output[7] = '*'; // upper 'c' symbol. Used modified TM16XXFonts.h
  } 
  else
  {
    for (i = 0; i < 4; i++) output[i+4] = ledData[i+1];
  }
  for (i = 0; i < 8; i++) { // convert data via Charmap
    if ((ledData[0] == '1') && (i >= 4)) break; // not need to convert, direct data
    output[i] = FONT_DEFAULT[output[i] - 32];
  }
  (rtcTime.sec % 2) ? bitSet(output[1], 7) : bitClear(output[1], 7); // draw/clear dot (seconds indicator)
  led.setDisplay(output, 8); // update led

  // Update bulbs
  if (bulbCurrent != bulbPrev) {
    bulbPrev = bulbCurrent;
    led.setLEDs(bulbState[bulbPrev]); // TO-DO: Check correctly
  }

  keys = led.getButtons();   // Read key state
  if (keys != lastKeys) {    // Key state changed. Send to UART
    Serial.print("K");
    Serial.println(keys); //
    lastKeys = keys;
  }
}

void updatePins(char pin, int val) { // update Output pins (PWM and digital)
  int paramM;
  switch (pin) {
    case '1':
      pwmValues[0] = val;
      Timer1.setPwmDuty(PWM1, pwmValue(val));
      break;
    case '2':
      pwmValues[1] = val;
      Timer1.setPwmDuty(PWM2, pwmValue(val));
      break;
    case '3':
      paramM = map(pwmValue(val), 0, 1023, 0, 255);
      pwmValues[2] = val;
      analogWrite(PWMLR, paramM);
      break;
    case 'A':
      digitalWrite(AUXOUT, val);
      break;
    case 'B':
      digitalWrite(BUZZER, val);
      break;
    case 'L':
      digitalWrite(BACKLIGHT, val);
  }
  
}


// ===========================================
void setup() {
  // setup pin modes...
  pinMode(UW_ECHO, INPUT);     
  pinMode(BELL, INPUT_PULLUP);
  pinMode(UW_TRIG, OUTPUT);      digitalWrite(UW_TRIG, LOW);
  pinMode(DOOR, INPUT_PULLUP);
  pinMode(PWMLR, OUTPUT);        digitalWrite(PWMLR, LOW);
  pinMode(AUXOUT, OUTPUT);       digitalWrite(AUXOUT, LOW);
  pinMode(BACKLIGHT, OUTPUT);    digitalWrite(BACKLIGHT, LOW);
  pinMode(BUZZER, OUTPUT);       digitalWrite(BUZZER, LOW);
  comparatorSetup();
  rtc.begin();
  // ....etc
  
  // setup interrupt handlers
  attachInterrupt(digitalPinToInterrupt(UW_ECHO), handleUS, CHANGE);
  attachInterrupt(digitalPinToInterrupt(BELL), handleBell, FALLING);
  attachPinChangeInterrupt(DOOR, handleDoor, CHANGE);

  //
  bufferData.reserve(64);
  Serial.begin(9600); // set UART speed
  Serial.setTimeout(100); // set timeout for Serial.parseInt
  Timer1.initialize(timerInterval); // 50 microseconds = 20 kHz frequency
  Timer1.attachInterrupt(handleTimer);
  Timer1.pwm(PWM1, 0); 
  Timer1.pwm(PWM2, 0);
  
  // setup DS18B20
  DeviceAddress tempAddr;
  ds.begin();
  ds.getAddress(tempAddr, 0);
  ds.setResolution(tempAddr, tempResolution);
  
  // Beep before begin 
  for (int i = 0; i < 3; i++) {
    digitalWrite(BUZZER, HIGH);
    delay(200);
    digitalWrite(BUZZER, LOW);
    delay(200);
  }
  Timer1.start();
  Serial.println("!RDY");
}


// ============================================
void loop() {
  int i;
  String S = "";
  // ---------- Event Handlers ---------
  
  if (irUSecho) { // UltraSonic Echo event
    if (distance() < ringDistance) {
      Serial.print("U");
      Serial.println(distance());
    }
    //
    irUSecho = false;
  }

  if (ifUStrig) {
    digitalWrite(UW_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(UW_TRIG, LOW);
    ifUStrig = false;
  }

  if (irBell) {
    Serial.println("B");
    irBell = false;
  }
  
  if (irDoor) {
    Serial.print("D");
    Serial.println(digitalRead(DOOR)?"1":"0");
    irDoor = false;
  }

  // ---------- PIR state ------------
  int PIRvalue = analogRead(PIR);
  if (!PIRstate && (PIRvalue > maxThreshold)) {
    Serial.println("P1");
    PIRstate = true;
  }
  if (PIRstate && (PIRvalue < minThreshold)) {
    Serial.println("P0");
    PIRstate = false; 
  }

  if (ifTemp) { // Temp conversion in progress. Done?
    if (millis() - tempRequest >= tempDelay) {
      temp = ds.getTempCByIndex(0);
      Serial.print("T"); 
      Serial.println(temp, 2); // send temperature to UART
      ifTemp = false; // reset conversion flag
    }
  }
  // ----------- Send collected data to UART ------
  if (ifSend) { // Send to UART: Light level, Cmp counter, Cmp Interval, RTC, RTC temp, Temp
    Serial.print("L"); // Light
    Serial.println(analogRead(LIGHT));
    noInterrupts(); // disable interrupts for operate with cmpCounter
    int cmpCounterInt = cmpCounter; // Temp variable
    long deltaMicrosCmpInt = deltaMicrosCmp;
    cmpCounter = 0;
    interrupts(); // enable interrupts
    Serial.print("C");
    Serial.println(cmpCounterInt);
    Serial.print("I");
    Serial.println(deltaMicrosCmpInt);
    Serial.print("i");
    // simplest Low Pass Filter (LPF)
    Serial.println((d1*80 + d2*10 + d3*6 + d4*3 + d5*1) / 100);  
    // send RTC
    sendRTC();
    // start Temp conversion
    ds.setWaitForConversion(false);
    ds.requestTemperatures();
    tempRequest = millis();
    ifTemp = true;    
    ifSend = false;
  }
  // --------- Receive data from UART -----------
  if (irCommand) {
    char bank;
    switch (bufferParseChar()) {
      case 'R':
        ifSend = true;
        break;
      case 'P':
        S = "";
        bank = bufferParseChar();
        while (true) { // read buffer until #0
          pc = bufferParseChar();
          if (pc == '\n') break;
          S += pc;
        }
        writeEEPROMString(bank, S);
        S = ""; // clear memory
        break;
      case 'G':
        bank = bufferParseChar();
        S = readEEPROMString(bank);
        Serial.print("G");
        Serial.print(bank);
        Serial.println(S);
        S = ""; // clear memory
        break;
      case 'D':
        p1 = bufferParseInt();
        p2 = bufferParseInt();
        p3 = bufferParseInt();
        rtc.setDate(p3, p2, p1);
        break;
      case 'T':
        p1 = bufferParseInt();
        p2 = bufferParseInt();
        p3 = bufferParseInt();
        rtc.setTime(p1, p2, p3);
        break;
      case 'U':
        p1 = bufferParseInt();
        ringDistance = p1;
        break;
      case 'S':
        pc = bufferParseChar();
        p1 = bufferParseInt();
        updatePins(pc, p1); 
        break;
      case 'A':
        pc = bufferParseChar();
        i = pc - 49; // char(49) = '1'. Symbol '1' eq. to index 0, '2' eq. index 1, etc.
        bitSet(ifAnimate, i); // set animate pin bit
        p1 = bufferParseInt();
        pwmDest[i] = p1;
        break;
      case 'a':
        animatePerSecond = bufferParseInt();
        animateMax = timerFrequency/animatePerSecond;
        break;
      case 'B':
        p1 = bufferParseInt();
        led.setupDisplay(true, p1);
        break;
      case 'Z':
        buzzerData = bufferParseInt();
        ifBuzzer = true;
        break;
      case 'F':
        timerFrequency = bufferParseInt();
        Timer1.setPeriod(timerInterval);
        break;
      case  'L':
        pc = bufferParseChar();
        ledData[i] = pc;
        for (i = 1; i < 5; i++) {
          if (pc == '1') {
            p1 = bufferParseInt();
            ledData[i] = p1;
          } 
          else
          {
            pc = bufferParseChar();
            ledData[i] = pc;
          }
        }
        break;
      case 'l':
        for (i = 0; i < 16; i++) {
          p1 = bufferParseInt();
          bulbState[i] = p1;
        }
    }
    bufferClear();
    irCommand = false;
  }
  updateLed();
  if (ifBuzzer) updateBuzzer();
  if ((ifAnimate > 0) && (ifAnimateStep)) updateAnimate();
}

// ============================================
void serialEvent() {
  if (irCommand) return;
  while (Serial.available()) {
    char inChar = (char)(Serial.read());
    bufferData += inChar;
    if (inChar == '\n')
      irCommand = true;
  }
}

// ============================================
// Interrupt handlers

void handleUS() { // Ultrasonic interrupt handler
  switch (digitalRead(UW_ECHO)) {
    case HIGH:
      startMicrosUS = micros();
      break;
    case LOW:
      stopMicrosUS = micros();
      irUSecho = true;
  }
}

void handleBell() { // Bell interrupt handler
  irBell = true;
}

void handleDoor() { // Door change state interrupt handler
  irDoor = true;
}

void handleTimer() { // Timer 1 interrupt handler
  USscans++;
  if (USscans >= USscansMax) {
    USscans = 0;
    ifUStrig = true;
  } 
  sendCounter++;
  if (sendCounter >= sendIntervalMax) {
    sendCounter = 0;
    ifSend = true;
  }
  bulbsCounter++;
  if (bulbsCounter >= bulbsRotateMax) {
    bulbsCounter = 0;
    bulbCurrent++;
    if (bulbCurrent > 15) bulbCurrent = 0;
  }
  buzzerCounter++;
  if (buzzerCounter >= buzzerRotateMax) {
    buzzerCounter = 0;
    if (ifBuzzer) buzzCurrent++;
  }
  animateCounter++;
  if (animateCounter >= animateMax) {
    animateCounter = 0;
    ifAnimateStep = true;
  }
}

ISR(ANALOG_COMP_vect) { // Comparator interrupt
  // if ACIS0 = 0 and ACIS1 = 0 then use STATE = ACSR & _BV(ACO)
  long currMicros = micros();
  cmpCounter++;
  deltaMicrosCmp = currMicros - prevMicrosCmp;
  prevMicrosCmp = currMicros;
  // Approx. for LPF
  d5 = d4;
  d4 = d3;
  d3 = d2;
  d2 = d1;
  d1 = deltaMicrosCmp;
}




